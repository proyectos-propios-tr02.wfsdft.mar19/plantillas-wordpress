<?php
/*
Template Name: Asociacion
*/
?>
<?php get_header(); ?>


<div id="contenedor_contenido" class="clearfix">    
    <div class="container_16 clearfix">
    


<div id="form_contact" class="grid_14 clearfix">



<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
        
        <div id="post-<?php the_ID(); ?>">
        
        
        <fieldset>
		<legend><?php _e('Old Pupil Association', 'wpml_theme'); ?></legend>
        
        
        
        
        <?php the_content(''); ?>
        
        </fieldset>
        
        </div>
        
        <?php endwhile; ?>

		<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>

</div>



</div> <!--end contenedor contenido -->


<?php get_footer(); ?>