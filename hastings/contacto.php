<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>


<div id="contenedor_contenido" class="clearfix">    
    <div class="container_16 clearfix">
    


<div id="form_contact" class="grid_14 clearfix">



<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
        
        <div id="post-<?php the_ID(); ?>">
        
        
        <fieldset>
		<legend><?php _e('Contact with Us', 'wpml_theme'); ?></legend>
        
        <div id="contacto_dir" class="grid_3">
        <p><strong>NURSERY AND INFANTS SCHOOL:</strong></p>
        <p>Tel. 91 359 06 21</p>
        
        <p><strong>JUNIOR AND SECONDARY SCHOOL</strong></p>
        <p>Tel. 91 359 99 13</p>
        </div>
        
        <div id="form_contact_datos" class="grid_9">
        <?php the_content(''); ?>
        </div>
        </fieldset>
        
        </div>
        
        <?php endwhile; ?>

		<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>

</div>



</div> <!--end contenedor contenido -->


<?php get_footer(); ?>