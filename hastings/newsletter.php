<?php
/*
Template Name: Newsletters
*/
?>
<?php get_header(); ?>


<div id="contenedor_contenido" class="clearfix">    
    <div class="container_16 clearfix">
    


<div id="newsletter_wrapper" class="grid_15 clearfix">
	
<div id="contenido" class="grid_15 alpha">
	

<?php if (have_posts()) : ?>
		<?php query_posts('cat=8'); ?>

		<?php while (have_posts()) : the_post(); ?>
        
        <div class="newsletter_item clearfix">
        <h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
        <small><?php the_time('F jS, Y') ?></small>
        
        
        <?php the_content('More'); ?>
		
        <p><a href="<?php echo get('fich_newsletter'); ?>"><img src="<?php bloginfo("template_url"); ?>/img/download_file.png"  /></a></p>
       
        
        
        
        </div>
        
        <?php endwhile; ?>

		<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>

</div>

</div>		






</div>
</div> <!--end contenedor contenido -->


<?php get_footer(); ?>