<?php get_header(); ?>


<div id="contenedor_contenido" class="clearfix">    
    <div class="container_16 clearfix">
    
<?php include(TEMPLATEPATH."/sidebar_left_news.php");?>

<div id="contenido" class="grid_12">
	

<?php if (have_posts()) : ?>
		

		<?php while (have_posts()) : the_post(); ?>
        
        <div class="pagina_noticia clearfix">
        <h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
        <small><?php the_time('F jS, Y') ?></small>
        
        
        <div class="grid_12 alpha">
        <?php $img_01 = get_image('imagen_01');
		$img_02 = get_image('imagen_02');
		 ?>
        <?php if($img_01 !== '') { 
		echo "<div class='grid_4 alpha'>";
		echo get_image('imagen_01');
		echo get_image('imagen_02');
		echo "</div>";
		echo "<div class='grid_7 omega'>";
		the_content('More');
		echo "</div>";
		}
		else {the_content('More');}
		?> 
        
        </div>
        </div>
        
        <?php endwhile; ?>

		<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>

</div>



</div> <!--end contenedor contenido -->


<?php get_footer(); ?>