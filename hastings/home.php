<?php
/*
Template Name: Home
*/
?>


<?php get_header(); ?>


<div id="contenedor_contenido" class="clearfix">    
    <div class="container_16 clearfix">
    
<?php include(TEMPLATEPATH."/sidebar_left_home.php");?>

<div id="contenido" class="grid_12">
	<div id="proyeccion">
   		<?php $languages = icl_get_languages('skip_missing=0');
		$template_url=get_bloginfo('template_url');
		if ( $languages[en][ 'active'])
		{
    	echo '<a href=""><img src="'.$template_url.'/img/hastings_school_since_1971.jpg" /></a>';
        echo '<a href=""><img src="'.$template_url.'/img/early_years.jpg" /></a>';
        echo '<a href=""><img src="'.$template_url.'/img/primary.jpg" /></a>';
        echo '<a href=""><img src="'.$template_url.'/img/primary_02.jpg" /></a>';
        echo '<a href=""><img src="'.$template_url.'/img/secondary.jpg" /></a>';
        echo '<a href=""><img src="'.$template_url.'/img/secondary_02.jpg" /></a>';
		$url_summer='http://www.hastingsschool.com/summer-programme-2011/';
		}
		
		elseif ( $languages[es][ 'active'])
				{
				echo '<a href=""><img src="'.$template_url.'/img/hastings_school_since_1971.jpg" /></a>';
        echo '<a href=""><img src="'.$template_url.'/img/infantil.jpg" /></a>';
        echo '<a href=""><img src="'.$template_url.'/img/primaria.jpg" /></a>';
        echo '<a href=""><img src="'.$template_url.'/img/primaria_02.jpg" /></a>';
        echo '<a href=""><img src="'.$template_url.'/img/secundaria.jpg" /></a>';
        echo '<a href=""><img src="'.$template_url.'/img/secundaria_02.jpg" /></a>';
		$url_summer='http://www.hastingsschool.com/programa-de-verano-2011/?lang=es';
			}
			
			
			?>
        
        </div>
        <div id="pager"></div>
        

</div>
</div> <!--end contenedor contenido -->



<!--News -->
<div id="noticias_central">
<div  class="container_16 clearfix">
	
    
    <div class="news_home_hoz_summer grid_4 alpha">
    <a href="<?php echo $url_summer; ?>"><img src="<?php bloginfo("template_url"); ?>/img/summer_programme03.jpg" /></a>
    <a href="http://www.hastingsschool.com/asociacion-de-antiguos-alumnos/?lang=es"><img src="<?php bloginfo("template_url"); ?>/img/asociacion_antiguos_alumnos.jpg" /></a>
    
            <a href="gallery-year-13/"><img src="<?php bloginfo("template_url"); ?>/img/childrens-work02.jpg" /></a>
            
	</div>
    
    <div id="noticias_home"  class="grid_12 omega">
    	<h1 class="news"><?php icl_link_to_element(300,'page',false); ?></h1>
    <?php query_posts('cat=3&showposts=1'); ?>
    <?php while (have_posts()) : the_post(); ?>
    <?php 
    echo "<div class='news_home_hoz grid_4'>";
	echo get_image('Imagen_thumb');
	echo "</div>";
    echo "<div class='news_home_hoz grid_7 omega'>";
	?>	
    	<h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>   
    <?php the_excerpt(''); ?>	
    <?php endwhile;?>
            
                 
    	</div>
        
     </div>   
    
    
</div><!--container 16 -->
</div><!--noticias_central -->
	

   
<div class="container_16 clearfix">
    <div class="grid_6">
    <h1><?php _e('Hastings&nbsp;School&nbsp;Programme', 'wpml_theme'); ?></h1>
    </div>  
</div><!--container 16 -->


<!-- This Month -->

<div class="container_16 clearfix">
    <div id="programa_home">
    <div class="news_home_hoz grid_5">
    		<h3><?php _e('Last Month', 'wpml_theme'); ?></h3>
    		<?php $query=query_posts('cat=4&showposts=1&offset=2'); ?>

			<?php while (have_posts()) : the_post(); ?>
			<?php the_content('More'); ?>
			<?php endwhile;?>
        	
    </div>
    
    <div class="news_home_hoz grid_5">
    		<h3><?php _e('This Month', 'wpml_theme'); ?></h3>
    		<?php query_posts('cat=4&showposts=1&offset=1'); ?>

			<?php while (have_posts()) : the_post(); ?>
			<?php the_content('Continua Leyendo'); ?>
			<?php endwhile;?>
        	
    </div>
    
    <div class="news_home_hoz grid_5">
    		<h3><?php _e('Coming up', 'wpml_theme'); ?></h3>
    		<?php query_posts('cat=4&showposts=1'); ?>

			<?php while (have_posts()) : the_post(); ?>
			<?php the_content('Continua Leyendo'); ?>
			<?php endwhile;?>
        	
    </div>
    </div>
      
</div><!--container 16 -->


<?php get_footer(); ?>