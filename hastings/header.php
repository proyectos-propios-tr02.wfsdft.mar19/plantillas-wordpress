<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="icon" type="image/x-icon" href="<?php bloginfo("template_url"); ?>/img/favicon.ico" />
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo("template_url"); ?>/img/favicon.ico" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/scripts/jquery-1.4.1.min.js"></script> 
<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/scripts/jquery.cycle.min.js"></script>
<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/scripts/sifr.js"></script> 
<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/scripts/jquery.galleria.js"></script>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#proyeccion').cycle({ 
			cleartype: false,
		    speed:  1000, 
		    timeout:  8550,
		    pager:  '#pager',
			random:  0
		});
		
		// Extend Footer
   var DivHeight = $("#wrapper").height();
   var WindowHeight=$(window).height();
   var FooterHeight = $("#extend_footer").height();
   var diff=0;
   var FootExt = 0;
   
   if ( WindowHeight > DivHeight)
		{
		diff=WindowHeight - DivHeight;
		FootExt = diff + FooterHeight;
		
		//alert (newcss);
		$("#extend_footer").css("height",FootExt);
		}
   //alert (DivHeight);
   //alert ($(window).height());
	
	
	});
</script>
<script type="text/javascript">
if(typeof sIFR == "function"){
    sIFR.replaceElement("h1", named({sFlashSrc: "<?php bloginfo("template_url"); ?>/achancery.swf", sWmode:"transparent", sColor: "#72817b"}));
};
</script>

<!--[if lte IE 6]>
	<link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/css/ie6.css">
<![endif]-->

<?php wp_head(); ?>
</head>

<body>
<div id="wrapper">

<div id="cabecera" class="clearfix">
	<div class="container_16 clearfix">
	<div id="logo" class="grid_4">
    <a href="<?php bloginfo('url');?>"></a>
    </div>
    
    <div class="grid_1 prefix_10">
    		<?php $languages = icl_get_languages('skip_missing=0');?>
            
            <a href="<?php echo $languages[en][ 'url'];?>"><img src="<?php bloginfo("template_url"); ?>/img/bandera_uk.gif" /></a>
            </div>
            <div class="grid_1">
            <a href="<?php echo $languages[es]['url'];?>"><img src="<?php bloginfo("template_url"); ?>/img/bandera_esp.gif" /></a>
            </div>
    
    </div><!--end container 16 -->
    
</div><!-- end cabecera -->



<!--menu -->

  <div id="menu" class="clearfix">
    <div class="container_16 clearfix">
   
    
    <ul>
    
     
    <li><?php icl_link_to_element(139,'page',false); ?></li>
    <li><?php icl_link_to_element(168,'page',false); ?></li>
    <li><?php icl_link_to_element(172,'page',false); ?></li>
    <li><?php icl_link_to_element(192,'page',false); ?></li>
    <li><?php icl_link_to_element(200,'page',false); ?></li>
    <li><?php icl_link_to_element(214,'page',false); ?></li>
    <li><?php icl_link_to_element(222,'page',false); ?></li>
    <li><?php icl_link_to_element(230,'page',false); ?></li>
    <li><?php icl_link_to_element(275,'page',false); ?></li>
    
    <li><?php icl_link_to_element(239,'page',false); ?></li>
    
    
    </ul>
    
    </div><!--end menu --> 
    </div><!--end container 16 -->
  


