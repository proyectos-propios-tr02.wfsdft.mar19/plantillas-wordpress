<div id="navegacion_izq" class="grid_4 alpha">
        
        
		
        <h1><?php _e('Downloads', 'wpml_theme'); ?></h1>
        	<ul id="navlist_left">
      		<?php $languages = icl_get_languages('skip_missing=0');
			 $url_blog=get_bloginfo('siteurl');
            if ( $languages[en][ 'active'])
				{
				echo '<li id="active"><a href="http://www.hastingsschool.com/pdf/hastings.pdf">General Info</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/ski-trip-2011/">Ski Trip</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/pdf/menu_ secondary_2007_8.pdf">Menu</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/pdf/prenursery_eng.pdf">Prenursery Info</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/pdf/EXTRA_ACTIVITIES_FORM_2010_11.pdf">Extra Curricular activities</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/pdf/CALENDARIO-HASTINGS-2010-11.pdf">School Calendar 2010/2011</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/pdf/procedures_policies.pdf">Procedures and Policies</a></li>';
				}
			elseif ( $languages[es][ 'active'])
				{
				echo '<li id="active"><a href="http://www.hastingsschool.com/pdf/hastings.pdf">Informaci&oacute;n General</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/viaje-de-ski-2011/?lang=es">Viaje de Ski</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/pdf/menu_ secondary_2007_8.pdf">Menu</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/pdf/prenursery_esp.pdf">Informaci&oacute;n Prenursery</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/pdf/EXTRA_ACTIVITIES_FORM_2010_11.pdf">Actividades Extracurriculares</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/pdf/CALENDARIO-HASTINGS-2010-11.pdf">Calendario escolar 2010/2011</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/pdf/procedures_policies.pdf">Procedures and Policies</a></li>';
				}
			
			
			?>
        
        	
			
            
			</ul>
            
        	     
        </div>
	