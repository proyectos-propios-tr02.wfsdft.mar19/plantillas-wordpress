<?php
/*
Template Name: Sitemap
*/
?>

<?php get_header(); ?>


<div id="contenedor_contenido" class="clearfix">    
    <div class="container_16 clearfix">
    


<div id="sitemap_wrapper" class="grid_15 clearfix">
	

<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
        
        
        <h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
        
       <div class="sitemap_item grid_5">
       	<ul>
		 <li><?php icl_link_to_element(139,'page',false); ?></li>
  		  <li><?php icl_link_to_element(168,'page',false); ?></li>
 	   	  <li><?php icl_link_to_element(172,'page',false); ?></li>
  	  	  <li><?php icl_link_to_element(192,'page',false); ?></li>
  	 	  <li><?php icl_link_to_element(200,'page',false); ?></li>
  	 	  <li><?php icl_link_to_element(214,'page',false); ?></li>
   		  <li><?php icl_link_to_element(222,'page',false); ?></li>
   		  <li><?php icl_link_to_element(230,'page',false); ?></li>
   		 <li><?php icl_link_to_element(275,'page',false); ?></li>
    	 <li><?php icl_link_to_element(239,'page',false); ?></li>
         </ul>
		</div>
        
        <div class="sitemap_item grid_5">
        <ul>
			
			<?php $languages = icl_get_languages('skip_missing=0');
			 $url_blog=get_bloginfo('siteurl');
            if ( $languages[en][ 'active'])
				{
				echo '<li id="active"><a href="../pdf/hastings.pdf">General Info</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/summer2010/index.html" id="current">Summer Activities 2010</a></li>';
				echo '<li><a href="../pdf/pdf_baqueira_2010.pdf">Ski Trip</a></li>';
				echo '<li><a href="../pdf/menu_ secondary_2007_8.pdf">Menu</a></li>';
				echo '<li><a href="../pdf/prenursery_eng.pdf">Prenursery Info</a></li>';
				echo '<li><a href="../pdf/pdf_extra_curricular_act_2010.pdf">Extra Curricular activities</a></li>';
				}
			elseif ( $languages[es][ 'active'])
				{
				echo '<li id="active"><a href="../pdf/hastings.pdf">Informaci&oacute;n General</a></li>';
				echo '<li><a href="http://www.hastingsschool.com/summer2010/index_esp.html" id="current">Actividades de Verano 2010</a></li>';
				echo '<li><a href="../pdf/pdf_baqueira_2010.pdf">Viaje de Ski</a></li>';
				echo '<li><a href="../pdf/menu_ secondary_2007_8.pdf">Menu</a></li>';
				echo '<li><a href="../pdf/prenursery_esp.pdf">Informaci&oacute;n Prenursery</a></li>';
				echo '<li><a href="../pdf/pdf_extra_curricular_act_2010.pdf">Actividades Extracurriculares</a></li>';
				}
			
			
			?>
		</ul>
        </div> 
        
        <div class="sitemap_item grid_5">
        <ul>
       		 <li><?php icl_link_to_element(300,'page',false); ?></li>
			 <li><?php icl_link_to_element(383,'page',__('Old Pupil Association')); ?></li>
			<li><?php icl_link_to_element(280,'page',false); ?></li>
		</ul>
        </div> 
        
       
        
        <?php endwhile; ?>


	<?php endif; ?>

</div>



</div> <!--end contenedor contenido -->


<?php get_footer(); ?>