<?php get_header(); ?>


<div id="contenedor_contenido" class="clearfix">    
    <div class="container_16 clearfix">
    
<?php include(TEMPLATEPATH."/sidebar_left_news.php");?>

<div id="contenido" class="grid_12">
	

<?php if (have_posts()) : ?>
		
		

		<?php while (have_posts()) : the_post(); ?>
        
        <div class="pagina_noticia clearfix">
        <h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
        <small><?php the_time('F jS, Y') ?></small>
        
        
        <?php $Thumbnail = get_image('Imagen_thumb'); ?>
        <?php if($Thumbnail !== '') { 
		echo "<div class='grid_7'>";
		the_content('More');
		echo "</div>";
		echo "<div class='grid_4'>";
		echo get_image('Imagen_thumb');
		echo "</div>";
		}
		else {the_content('More');}
		?> 
        
       
        
        
        
        </div>
        
        <?php endwhile; ?>

		<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>

</div>



</div> <!--end contenedor contenido -->


<?php get_footer(); ?>