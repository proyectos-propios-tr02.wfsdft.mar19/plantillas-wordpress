<?php
/*
Template Name: Gallery
*/
?>
<?php get_header(); ?>

<script type="text/javascript"> 

$(document).ready(function(){
		
		$('.gallery_unstyled').addClass('galeria_hastings'); // adds new class name to maintain degradability
		
		$('ul.galeria_hastings').galleria({
			history   : true, // activates the history object for bookmarking, back-button etc.
			clickNext : true, // helper for making the image clickable
			insert    : '#main_image_gallery', // the containing selector for our main image
			onImage   : function(image,caption,thumb) { // let's add some image effects for demonstration purposes
				
				// fade in the image & caption
				image.css('display','none').fadeIn(1000);
				caption.css('display','none').fadeIn(1000);
				
				// fetch the thumbnail container
				var _li = thumb.parents('li');
				
				// fade out inactive thumbnail
				_li.siblings().children('img.selected').fadeTo(500,0.3);
				
				// fade in active thumbnail
				thumb.fadeTo('fast',1).addClass('selected');
				
				// add a title for the clickable image
				image.attr('title','Next image >>');
			},
			onThumb : function(thumb) { // thumbnail effects goes here
				
				// fetch the thumbnail container
				var _li = thumb.parents('li');
				
				// if thumbnail is active, fade all the way.
				var _fadeTo = _li.is('.active') ? '1' : '0.3';
				
				// fade in the thumbnail when finnished loading
				thumb.css({display:'none',opacity:_fadeTo}).fadeIn(1500);
				
				// hover effects
				thumb.hover(
					function() { thumb.fadeTo('fast',1); },
					function() { _li.not('.active').children('img').fadeTo('fast',0.3); } // don't fade out if the parent is active
				)
			}
		});
	});
	
</script>



<div id="contenedor_contenido" class="clearfix">    
    <div class="container_16 clearfix">
    
<?php include(TEMPLATEPATH."/sidebar_left_gallery.php");?>

<div id="contenedor_galeria" class="grid_12 omega">
	

<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
        
        <div id="post-<?php the_ID(); ?>">
        
        
        <div class="demo">
		<div id="main_image_gallery"></div>
		<ul class="gallery_unstyled">
        <li class="active"><img src="<?php echo get('gal_img_01'); ?>" title="<?php echo get('txt_img_01'); ?>" /></li>
        <li><img src="<?php echo get('gal_img_02'); ?>" title="<?php echo get('txt_img_02'); ?>" /></li>
        <li><img src="<?php echo get('gal_img_03'); ?>" title="<?php echo get('txt_img_03'); ?>" /></li>
        <li><img src="<?php echo get('gal_img_04'); ?>" title="<?php echo get('txt_img_04'); ?>" /></li>
        </ul> 

	    </div>
        
        </div>
        
        <?php endwhile; ?>

		<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>

</div>



</div> <!--end contenedor contenido -->


<?php get_footer(); ?>