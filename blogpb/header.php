<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="icon" type="image/x-icon" href="<?php bloginfo("template_url"); ?>/favicon.ico" />
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo("template_url"); ?>/favicon.ico" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link href="http://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/scripts/jquery-1.4.1.min.js"></script> 
<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/scripts/jquery.corner.js"></script>


<!--[if lte IE 6]>
	<link rel="stylesheet" href="css/ie6.css">
	<script type="text/javascript">
		window.mlrunShim = true;
	</script>
<![endif]-->
<!--[if IE 7]>
	<link rel="stylesheet" href="css/ie.css">
	<script type="text/javascript">
		window.mlrunShim = true;
	</script>
<![endif]-->
<!-- script google analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-4070831-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- fin google analytics -->

<?php wp_head(); ?>
</head>

<body>



<div id="wrapper">
<div id="header">
			<div class="page">
				<a id="header_logo" href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}">
					<img class="logo" src="<?php bloginfo("template_url"); ?>/img/logo-paperboogie-new.png" alt=""  />
				</a>
				<div id="header_right">
                
                
                <ul id="header_links">
	
    
	<li id="header_link_sitemap"><a href="http://www.paperboogie.com" title="">Scrapbooking</a></li>
    <li id="header_link_sitemap"><a href="http://www.paperboogie.com" title="">Accesorios</a></li>
    
    
    <li id="header_link_sitemap"><a href="http://www.paperboogie.com" title="">Packaging</a></li>
    
    
    <li id="header_link_contact"><a href="http://www.paperboogie.com" title="">Sobres</a></li>
    
	<li id="header_link_sitemap"><a href="http://www.paperboogie.com" title="">Papel Japones</a></li>
    
    
</ul>
                
                
					
				</div>
			</div>
		</div>

<div id="contenedor_contenido" class="clearfix">








<div class="container_24 clearfix">
	<div id="menu">	
        <ul> 
		
    	</ul>
	</div><!--end menu -->
    
</div><!--end container 24 -->



</div><!--end cont. cabecera -->

<div class="container_24 clearfix">
<div id="contenedor_cabecera">