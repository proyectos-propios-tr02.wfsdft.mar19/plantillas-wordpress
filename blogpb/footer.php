</div> <!--end contenedor contenido -->
</div>

<div id="footer">
<div id="contenedor_footer">


<div class="container_24 clearfix">

				<div class="footer_block grid_5">
        <h3><a href="http://www.paperboogie.com/es">Papeles creativos</a></h3>
		<ul>
		<li><a href="http://www.paperboogie.com/es/5-comprar-papel-japones" id="current">Papel japones</a></li>
        <li><a href="http://www.paperboogie.com/es/11-comprar-papel-para-scrapbooking" id="current">Papel Scrapbook</a></li>
		<li><a href="http://www.paperboogie.com/es/27-papel-artesano">Papel artesano</a></li>
        <li><a href="http://www.paperboogie.com/es/26-papel-origami" id="current">Papel para origami</a></li>

		</ul>
        
        </ul>
        </div><!--end menu footer invitaciones -->

		<div class="footer_block grid_5">
        <h3><a href="http://www.paperboogie.com/es/13-comprar-etiquetas-cajas-packaging-manualidades">Packaging creativo</a></h3>
        <ul>
         <li><a href="http://www.paperboogie.com/es/13-comprar-etiquetas-cajas-packaging-manualidades">Cajitas kraft</a></li>

        <li><a href="http://www.paperboogie.com/es/15-cintas-para-scrapbooking-y-manualidades">Bakers Twine y cintas</a></li>

        </ul>
        </div><!--end menu footer invitaciones -->
        
        
        <div class="footer_block grid_5">
        <h3><a href="http://www.paperboogie.com/es/8-comprar-accesorios-scrapbooking">Scrapbook</a></h3>
        <ul>
        <li><a href="http://www.paperboogie.com/es/16-comprar-washi-tape-craft-tape-y-fabric-tape">Washi Tape</a></li>
        <li><a href="http://www.paperboogie.com/es/22-comprar-sellos-scrapbooking">Sellos</a></li>

        </ul>
        </div><!--end menu footer invitaciones -->
        
        <div class="footer_block grid_5">
        <h3><a href="http://www.paperboogie.com/es/29-promociones">Promociones</a></h3>
        <ul>

        <li><a href="http://www.paperboogie.com/es/nuevos-productos">Novedades</a></li>
      
        </ul>
        </div><!--end menu footer invitaciones -->


        
        
</div> <!--end container 24 -->


<div class="container_24 clearfix">
	<div id="footer_copy">
	&copy; 2012-2015, paperboogie.com. Todos los derechos reservados.

	</div><!-- end footer copy-->
    
    

</div> <!--end container 24 -->


</div><!--contenedor footer -->
</div> <!--bg_footer -->



</div> <!-- end wrapper -->

<?php wp_footer(); ?>
</body>
</html>
