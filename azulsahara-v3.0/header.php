<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="icon" type="image/x-icon" href="<?php bloginfo("template_url"); ?>/favicon.ico" />
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo("template_url"); ?>/favicon.ico" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link href="http://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/scripts/jquery-1.4.1.min.js"></script> 
<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/scripts/jquery.corner.js"></script>


<!--[if lte IE 6]>
	<link rel="stylesheet" href="css/ie6.css">
	<script type="text/javascript">
		window.mlrunShim = true;
	</script>
<![endif]-->
<!--[if IE 7]>
	<link rel="stylesheet" href="css/ie.css">
	<script type="text/javascript">
		window.mlrunShim = true;
	</script>
<![endif]-->
<!-- script google analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-4070831-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- fin google analytics -->

<?php wp_head(); ?>
</head>

<body>



<div id="wrapper">


<div id="contenedor_contenido" class="clearfix">

<div id="contenedor_cabecera">
<div class="container_24 clearfix">

	<div id="cabecera" class="clearfix">
    
    <div id="logo_cabecera_eng" class="grid_18"></div>
    
    <div id="menu_header_redes_sociales" class="grid_6">
    
    	 <div id="contenedor_idiomas" class="display_inline">
         <?php $languages = icl_get_languages('skip_missing=0');?>
         
         
    <a href="<?php echo $languages[en][ 'url'];?>">English</a> | <a href="<?php echo $languages[es][ 'url'];?>">Espa&ntilde;ol</a>
    
    
    </div><!-- end contenedor idiomas -->
		
        <div id="contenedor_redes_sociales" class="display_inline">
 		<div id="suscribete" class="clearfix">Follow us:</div>
	   <div id="menu_suscripcion_azs" class="clearfix">
      
       
    <ul>
    	<li class="facebook"><a href="http://www.facebook.com/azulsahara" target="_blank">Facebook</a></li>
        <li class="twitter"><a href="http://twitter.com/azulsaharapaper" target="_blank">Twitter</a></li>
    	<li class="RSS"><a href="<?php bloginfo('rss2_url'); ?>" title="<?php _e('Suscribete usando RSS'); ?>" target="_blank">RSS</a></li>
    	
    </ul>
    
    
    </div><!-- end menu suscripcion-->
    
    </div><!-- /contenedor_redes_sociales -->
   
    
</div>
   
    </div><!--end cabecera -->

</div><!--end container 24 -->


<div class="container_24 clearfix">
	<div id="separador_menu_sup" class="display_inline"></div>
</div><!--end container 24 -->


<div class="container_24 clearfix">
	<div id="menu">	
        <ul> 
		<li id="<?php _e('menu-invitaciones', 'wpml_theme'); ?>" class="dir"><a href="<?php _e('http://www.azulsahara.com/5-invitaciones-de-boda', 'wpml_theme'); ?>"><span>Invitaciones de Boda</span></a>
        	  
        </li>
   		<li id="<?php _e('menu-detalles', 'wpml_theme'); ?>" class="dir"><a href="<?php _e('http://www.azulsahara.com/38-detalles-de-boda', 'wpml_theme'); ?>"><span>Detalles y Complementos</span></a></li>
        
        <li id="<?php _e('menu-press', 'wpml_theme'); ?>" class="dir"><a href="http://www.azulsahara.com/content/18-press"><span>Press</span></a></li>
        
    	</ul>
	</div><!--end menu -->
    
</div><!--end container 24 -->

<div class="container_24 clearfix">
	<div id="separador_menu_sup_dw" class="display_inline"></div>
</div><!--end container 24 -->

</div><!--end cont. cabecera -->