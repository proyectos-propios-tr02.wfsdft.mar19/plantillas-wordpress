<div id="contenedor_footer">

<div class="container_24 clearfix">
	<div class="separacion"></div>
</div><!--end container 24 -->
<div class="container_24 clearfix">

				<div class="menu_1_footer grid_5">
        <h3><a href="#">Vous avez besoin d&#039;aide?</a></h3>

		<ul>
		<li><a href="http://www.azulsahara.com/content/8-faq" id="current">Comment acheter</a></li>
        <li><a href="http://www.azulsahara.com/content/14-azulsahara-promotions" id="current">Promotions</a></li>
		<li><a href="http://www.azulsahara.com/content/17-Creation-faire-parts-mariage">Cr&eacute;ation de faire-parts de mariage</a></li>

        <li><a href="https://www.azulsahara.com/contact-form.php" id="current">Contactez-nous</a></li>
		</ul>
        
        </ul>
        </div><!--end menu footer invitaciones -->

		<div class="menu_1_footer grid_5">
        <h3><a href="#">Faire-parts de mariage</a></h3>

        <ul>
        <li><a href="http://www.azulsahara.com/52-collection-azulsahara">Collection Azulsahara</a></li>
        <li><a href="http://www.azulsahara.com/24-collection-metropolitan">Collection Metropolitan</a></li>
        <li><a href="http://www.azulsahara.com/22-collection-mumbai">Collection Mumbai</a></li>
        <li><a href="http://www.azulsahara.com/13-collection-ensuenos">Collection Ensue&ntilde;os</a></li>
        <li><a href="http://www.azulsahara.com/20-collection-kyoto">Collection Kyoto</a></li>
        <li><a href="http://www.azulsahara.com/7-collection-royal-bouquet">Collection Royal Bouquet</a></li>  
        <li><a href="http://www.azulsahara.com/16-collection-extravaganza">Collection Extravaganza</a></li>
        <li><a href="http://www.azulsahara.com/6-collection-armonia">Collection Armonia</a></li>
        <li><a href="http://www.azulsahara.com/23-collection-nature">Collection Nature</a></li>
       
        </ul>

        </div><!--end menu footer invitaciones -->
        
        
        <div class="menu_1_footer grid_5">
        <h3><a href="#">D&eacute;tails et compl&eacute;ments</a></h3>
        <ul>
        <li><a href="http://www.azulsahara.com/38-details-pour-les-invites">D&eacute;tails pour les invit&eacute;s et livre d&#039;or</a></li>

        <li><a href="http://www.azulsahara.com/8-cartes-remerciements">Cartes de remerciements</a></li>
        <li><a href="http://www.azulsahara.com/39-complementos-accesorios">Accessoires de faire-parts</a></li>
        </ul>
        </div><!--end menu footer invitaciones -->

        
        <div class="menu_1_footer grid_5">
        <h3><a href="#">About us</a></h3>
        <ul>
        <li><a href="http://www.azulsahara.com/content/4-sobre-azulsahara">About us</a></li>
        <li><a href="http://www.azulsahara.com/content/20-notre-travail">Notre travail</a></li>
        <li><a href="http://www.azulsahara.com/content/21-wedding-planners">Wedding Planners</a></li>

        <li><a href="http://www.azulsahara.com/content/13-Dossier-de-presse">Dossier de presse</a></li>
        <li><a href="http://www.azulsahara.com/content/2-legal-notice">Legal Notice</a></li>
        
        
        
        </ul>
        </div><!--end menu footer invitaciones -->
        
        



        
        
</div> <!--end container 24 -->


<div class="container_24 clearfix">
	<div id="footer_copy">
	&copy; 2008-2010, azulsahara.com. All rights reserved. <strong>Vous avez besoin d&#039;aide?</strong> Tel.: +34 91 855 63 90 <strong>e-mail</strong> info@azulsahara.com


	</div><!-- end footer copy-->
    
    

</div> <!--end container 24 -->


</div><!--contenedor footer -->



</div> <!--end contenedor contenido -->
</div> <!-- end wrapper -->

<?php wp_footer(); ?>
</body>
</html>
