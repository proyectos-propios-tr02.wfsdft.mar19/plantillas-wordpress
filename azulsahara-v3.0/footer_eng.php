</div> <!--end contenedor contenido -->

<div id="bg_footer">
<div id="contenedor_footer">

<div class="container_24 clearfix">
	<div class="separacion"></div>
</div><!--end container 24 -->
<div class="container_24 clearfix">

		<div class="menu_1_footer grid_5">
        <h3><a href="#">Need Help</a></h3>
		<ul>
		<li><a href="http://www.azulsahara.com/content/8-faq" id="current">How to order</a></li>

        <li><a href="http://www.azulsahara.com/content/14-azulsahara-promotions" id="current">Promotions</a></li>
		<li><a href="http://www.azulsahara.com/content/17-custom-wedding-stationery">Custom Wedding invitations</a></li>
        <li><a href="https://www.azulsahara.com/contact-form.php" id="current">Contact</a></li>
		</ul>
        
        </ul>
        </div><!--end menu footer invitaciones -->

		<div class="menu_1_footer grid_5">
        <h3><a href="#">Wedding Invitations</a></h3>
        <ul>
        <li><a href="http://www.azulsahara.com/52-azulsahara-collection">Azulsahara Collection</a></li>
        <li><a href="http://www.azulsahara.com/24-metropolitan-collection">Metropolitan Collection</a></li>
        <li><a href="http://www.azulsahara.com/22-mumbai-collection">Mumbai Collection</a></li>
        <li><a href="http://www.azulsahara.com/13-ensuenos-collection"></a></li>
        <li><a href="http://www.azulsahara.com/20-kyoto-collection">Kyoto Collection</a></li>
        <li><a href="http://www.azulsahara.com/7-royal-bouquet-collection">Royal Bouquet Collection</a></li>
        <li><a href="http://www.azulsahara.com/16-extravaganza-collection">Extravaganza Collection</a></li>
        <li><a href="http://www.azulsahara.com/6-armonia-collection">Armonia Collection</a></li>
        <li><a href="http://www.azulsahara.com/23-nature-collection">Nature Collection</a></li>
        

        </ul>
        </div><!--end menu footer invitaciones -->
        
        
        <div class="menu_1_footer grid_5">
        <h3><a href="#">Wedding Stationery</a></h3>
        <ul>
        <li><a href="http://www.azulsahara.com/38-wedding-favours">Wedding Favours</a></li>
        <li><a href="http://www.azulsahara.com/8-thankyou-cards">Thank you cards</a></li>
        <li><a href="http://www.azulsahara.com/39-complementos-accesorios">Matching items for invitations</a></li>
        </ul>
        </div><!--end menu footer invitaciones -->
        
        <div class="menu_1_footer grid_5">
        <h3><a href="#">About us</a></h3>
        <ul>

        <li><a href="http://www.azulsahara.com/content/4-sobre-azulsahara">About us</a></li>
        <li><a href="http://www.azulsahara.com/content/20-azulsahara-process">Our Process</a></li>
        <li><a href="http://www.azulsahara.com/content/21-wedding-planners">Wedding Planners</a></li>
        <li><a href="http://www.azulsahara.com/content/13-press-release">Press release</a></li>
        <li><a href="http://www.azulsahara.com/content/2-legal-notice">Legal Notice</a></li>
                
        </ul>
        </div><!--end menu footer invitaciones -->


        
        
</div> <!--end container 24 -->


<div class="container_24 clearfix">
	<div id="footer_copy">
	&copy; 2008-2012, azulsahara.com. All rights reserved. <strong>Need Help?</strong> Tel.: +34 91 855 63 90 <strong>e-mail</strong> info@azulsahara.com


	</div><!-- end footer copy-->
    
    

</div> <!--end container 24 -->


</div><!--contenedor footer -->
</div> <!--bg_footer -->




</div> <!-- end wrapper -->

<?php wp_footer(); ?>
</body>
</html>
