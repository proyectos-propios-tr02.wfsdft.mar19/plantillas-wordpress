</div> <!--end contenedor contenido -->

<div id="bg_footer">
<div id="contenedor_footer">

<div class="container_24 clearfix">
	<div class="separacion"></div>
</div><!--end container 24 -->
<div class="container_24 clearfix">

				<div class="menu_1_footer grid_5">
        <h3><a href="#">&iquest;Te ayudamos?</a></h3>
		<ul>
		<li><a href="http://www.azulsahara.com/content/8-faq" id="current">Como comprar</a></li>
        <li><a href="http://www.azulsahara.com/content/14-promociones-azulsahara" id="current">Promociones</a></li>
		<li><a href="http://www.azulsahara.com/content/17-diseno-de-invitaciones-de-boda">Dise&ntilde;o de invitaciones de boda</a></li>
        <li><a href="https://www.azulsahara.com/contact-form.php" id="current">Contacta con nosotros</a></li>

		</ul>
        
        </ul>
        </div><!--end menu footer invitaciones -->

		<div class="menu_1_footer grid_5">
        <h3><a href="http://www.azulsahara.com/5-invitaciones-de-boda">Invitaciones de Boda</a></h3>
        <ul>
         <li><a href="http://www.azulsahara.com/52-coleccion-azulsahara">Colecci&oacute;n Azulsahara</a></li>
        <li><a href="http://www.azulsahara.com/24-coleccion-metropolitan">Colecci&oacute;n Metropolitan</a></li>

        <li><a href="http://www.azulsahara.com/22-coleccion-mumbai">Colecci&oacute;n Mumbai</a></li>
        <li><a href="http://www.azulsahara.com/13-coleccion-ensuenos">Colecci&oacute;n Ensue&ntilde;os</a></li>
        <li><a href="http://www.azulsahara.com/20-coleccion-kyoto">Colecci&oacute;n Kyoto</a></li>
        <li><a href="http://www.azulsahara.com/7-coleccion-royal-bouquet">Colecci&oacute;n Royal Bouquet</a></li>
        <li><a href="http://www.azulsahara.com/16-coleccion-extravaganza">Colecci&oacute;n Extravaganza</a></li>
        <li><a href="http://www.azulsahara.com/6-coleccion-armonia">Colecci&oacute;n Armonia</a></li>
        <li><a href="http://www.azulsahara.com/23-coleccion-nature">Colecci&oacute;n Nature</a></li>
        

        </ul>
        </div><!--end menu footer invitaciones -->
        
        
        <div class="menu_1_footer grid_5">
        <h3><a href="http://www.azulsahara.com/38-detalles-de-boda">Complementos</a></h3>
        <ul>
        <li><a href="http://www.azulsahara.com/38-detalles-de-boda">Detalles de boda</a></li>
        <li><a href="http://www.azulsahara.com/8-tarjetas-de-agradecimiento">Tarjetas de agradecimiento</a></li>

        </ul>
        </div><!--end menu footer invitaciones -->
        
        <div class="menu_1_footer grid_5">
        <h3><a href="#">Sobre azulsahara</a></h3>
        <ul>

        <li><a href="http://www.azulsahara.com/content/4-sobre-azulsahara">Sobre azulsahara</a></li>
        <li><a href="http://www.azulsahara.com/content/20-proceso-azulsahara">Nuestro Proceso</a></li>
        <li><a href="http://www.azulsahara.com/content/13-nota-prensa">Nota de Prensa</a></li>
      
        </ul>
        </div><!--end menu footer invitaciones -->


        
        
</div> <!--end container 24 -->


<div class="container_24 clearfix">
	<div id="footer_copy">
	&copy; 2008-2012, azulsahara.com. Todos los derechos reservados. <strong>&iquest;Te ayudamos?</strong> Tel.: +34 91 855 63 90 <strong>e-mail</strong> info@azulsahara.com

	</div><!-- end footer copy-->
    
    

</div> <!--end container 24 -->


</div><!--contenedor footer -->
</div> <!--bg_footer -->



</div> <!-- end wrapper -->

<?php wp_footer(); ?>
</body>
</html>
