<?php get_header(); ?>



<div id="contenedor_central" class="clearfix">
<div class="container_24 clearfix">

<?php include(TEMPLATEPATH."/sidebar.php");?>

	<div id="contenedor_posts" class="grid_18">
   
   <?php $numero_post = 0;  ?>  
   <?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
        <?php $numero_post = $numero_post + 1;?> 
   
    <div class="pagina_post clearfix">
    <div class="header_post clearfix">
        <div>
        <?php if ($numero_post == 1) {?>
    		<h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
        <?php }else{?>
        	 <h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
        <?php }?>
        <small class="metadata">
        <?php the_time('F jS, Y') ?> | <?php _e('Categoria: ', 'wpml_theme'); ?><?php the_category(' '); ?> |  <?php _e('Escrito por', 'wpml_theme'); ?> <a href="<?php bloginfo('url');?>/<?php the_author() ?>/">		<?php the_author() ?></a> |
        
        
        <?php $languages = icl_get_languages('skip_missing=0');
		if ( $languages[es][ 'active'])
		{
		comments_popup_link('0 Comentarios', '1 Comentario', '% Comentarios'); 
		}
		else
		{
		comments_popup_link('0 Comments', '1 Comment', '% Comments');
		}
		?>
        
        
        </small>
        </div>
    </div><!-- end header post -->
    
    <!--POST -->
    <?php $languages = icl_get_languages('skip_missing=0');
		if ( $languages[es][ 'active'])
		{
		the_content('Continua leyendo ...'); 
		}
		else
		{
		the_content('Read More ...');
		}
		?>
	
	
    
    <small class="metadata">
        <?php the_tags(); ?>
        </small>
    
    </div><!--pagina_post -->
	
    
    
    <!--<iframe src="http://www.facebook.com/plugins/like.php?href=<?php the_permalink();?>&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=35" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:35px;" allowTransparency="true"></iframe>-->
    
    <!--<div class="separador_post clearfix"></div> -->
    
    <?php comments_template(); // Get wp-comments.php template ?>
    <?php endwhile; ?>
		<?php wp_pagenavi(); ?>

	<?php endif; ?>
    
    
    </div>
    
    
    
</div><!--end container 16 -->
</div><!--central -->




<?php get_footer(); ?>

