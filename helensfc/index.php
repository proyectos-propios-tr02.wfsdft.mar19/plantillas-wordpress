<?php get_header(); ?>


<div id="contenedor_central" class="clearfix">

		<div id="contenedor_central_contenido" class="display_inline">
        
        
        
        <?php $numero_post = 0;  ?>  
   <?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
        <?php $numero_post = $numero_post + 1;?> 
   
    <div class="entrada_post clearfix">
    <div class="header_post clearfix">
        <div>
        <?php if ($numero_post == 1) {?>
    		<h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
        <?php }else{?>
        	 <h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
        <?php }?>
        <small class="metadata">
        <?php the_time('F jS, Y') ?> | <?php _e('Categoria: ', 'wpml_theme'); ?><?php the_category(' '); ?> |  <?php _e('Escrito por', 'wpml_theme'); ?> <a href="<?php bloginfo('url');?>/<?php the_author() ?>/">		<?php the_author() ?></a> |
        
        
       
        
        
        </small>
        </div>
    </div><!-- end header post -->
    
    <!--POST -->
    <?php the_content('Read More ...'); ?>
	
	
    
    <small class="metadata">
        <?php the_tags(); ?>
        </small>
    
    </div><!--pagina_post -->
	
    
    <?php endwhile; ?>
		

	<?php endif; ?>


        </div> <!-- /contenedor_central_contenido-->
        
        
        <?php get_sidebar(); ?>

        

</div> <!--end contenedor_central -->



<?php get_footer(); ?>
