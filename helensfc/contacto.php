<?php
/*
Template Name: Contacto
*/
?>
<?php get_header(); ?>


<div id="contenedor_central" class="clearfix">

		<div id="contenedor_central_contenido" class="display_inline">
        
        <?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
        <div class="pagina_form clearfix">
    		<div class="header_post clearfix">
        	</div>
        
        
         <div id="contenedor_form" class="display_inline">
        
        <div id="cabecera_form" class="display_inline">
        	<div id="logo_form" class="display_inline"></div>
            <div id="direccion_form" class="display_inline">
            <p>Alc&aacute;ntara 21, B 1, Madrid<br />
				674 387 924<br />
				helena@helensfuncorner.com</p>
            </div>
        
        </div> <!-- /cabecera_form -->
          
        <div id="contenido_form" class="display_inline"> 
        
        
        <?php the_content('Read More ...');?>
        
         	
            <p class="disclaimer">
        Todas las actividades deber&aacute;n reservarse los martes o jueves de la semana anterior en horario de 10.30 a 14h en el tel&eacute;fono 674 387 924. M&iacute;nimo de ni&ntilde;s por taller 5, un m&aacute;ximo de 12/15. En caso de no haber n&uacute;meros suficientes se avisar&aacute; a los padres el jueves anterior por la tarde.
        </p>
                
                </div> <!-- /contenido form -->
                    
                    
                 </div><!--/ contenedor_form-->
        
        </div><!--pagina_post -->
        
        <?php endwhile; ?>


	<?php endif; ?>


        </div> <!-- /contenedor_central_contenido-->
        
        
        <?php get_sidebar(); ?>

        

</div> <!--end contenedor_central -->



<?php get_footer(); ?>
