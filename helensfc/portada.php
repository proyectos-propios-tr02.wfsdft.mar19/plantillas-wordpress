<?php
/*
Template Name: Portada
*/
?>
<?php get_header(); ?>


<div id="contenedor_central" class="clearfix">

		<div id="contenedor_central_contenido" class="display_inline">
        
        <div id="welcome" class="display_inline"></div>
        
        <p class="portada"><span class="superbold">Papa ¿me lees un cuento?</span>, <span class="superbold">"somos piratas" </span> o <span class="superbold">"¡mira mama que dibujo tan bonito!" </span> son algunas imágenes familiares del aprendizaje y la infancia de nuestros hijos. La curiosidad, explorar con los sentidos, dejar volar la imaginación y la creatividad, en definitiva, <span class="superbold">APRENDER JUGANDO</span> es la mejor manera para que nuestros pequeños se desarrollen intelectual y emocionalmente. 
Con este propósito nace Helen´s Fun Corner, un pequeño rincón donde vuestros hij@s entraran en contacto con el inglés, el chino, el teatro o las actividades manuales y creativas en un entorno agradable, estimulante y de la mano de expertos profesionales, dedicados y entusiastas.<br /><br />

¡Ven a visitarnos a 'nuestro rincón' y disfruta aprendiendo con tus amigos!</p>
<br />

<p class="portada"><span class="superbold">"Dad will you read me a story ?", "Bunny is a pirate!" or "mum look at my lovely painting!" </span> are all familiar images of our childrens' childhood. Children are full of curiosity, they like to explore with all their senses, to listen to and create their own stories, to take part in lovely art&craft activities… LEARNING THROUGH PLAY IS FUN ! and the best we can offer them to help in their emotional and intellectual development.
This is our aim at Helen's Fun Corner. A small corner, where your children can experience English, Chinese, Theatre and Art&Craft activities in a happy, rich environment with the support and expertise of enthusiastic professional.<br /><br />

Come an join us in ' The Fun of Learning' at Helen´s Fun Corner !</p>


<p class="helena">Helena González Pennefather<br />
British Qualified Primary Teacher<br />	
Hertfordshire University</p>


        </div> <!-- /contenedor_central_contenido-->
        
        
        <?php get_sidebar(); ?>

        

</div> <!--end contenedor_central -->



<?php get_footer(); ?>
