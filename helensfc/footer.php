<div id="contenedor_footer" class="clearfix">

<div class="container_24 clearfix">
		<div id="footer_direccion" class="grid_24">
        
                
        <ul id="direccion-top">
          <li>Dirección:&nbsp;&nbsp;<a href="https://maps.google.com/maps?q=alcantara+21,+madrid&hl=en&sll=37.0625,-95.677068&sspn=40.817312,75.234375&t=h&hq=alcantara+21,&hnear=Madrid,+Community+of+Madrid,+Spain&z=14" target="_blank">Alcantara 21, B1, Madrid</a></li>

          <li class="phone">Teléfono:&nbsp;&nbsp;674 387 924</li>

          <li class="email">Contacto:&nbsp;&nbsp;<a href="mailto:helena@helensfuncorner.com">helena@helensfuncorner.com</a></li>

        </ul>
        
        </div>
</div><!--end container 24 -->

<div class="container_24 clearfix">

  <!--<div id="more_info_footer">
    <p>Todas las actividades deberán reservarse los martes o jueves de la semana anterior en horario de 10.30 a 14h en el teléfono 674 387 924. Mínimo de niñ@s por taller 5, un máximo de 12/15. En caso de no haber números suficientes se avisará a los padres el jueves anterior por la tarde.</p>
    </div>  -->  
     

</div><!--end container 24 -->

<div class="container_24 clearfix">


</div><!--end container 24 -->

</div><!--/contenedor_footer -->


</div> <!--end contenedor contenido -->
</div> <!-- end wrapper -->

<?php wp_footer(); ?>
</body>
</html>
