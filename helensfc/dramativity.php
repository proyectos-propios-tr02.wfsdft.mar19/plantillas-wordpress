<?php
/*
Template Name: Dramativity
*/
?>
<?php get_header(); ?>


<div id="contenedor_central" class="clearfix">

		<div id="contenedor_central_contenido" class="display_inline">
        
        <?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
        <div class="pagina_post clearfix">
    		<div class="header_post clearfix">
        	</div>
        
        <div id="welcome-dramativity" class="display_inline"></div>
        
        
        <?php the_content('Read More ...');?>
        
        <p class="disclaimer">
        Todas las actividades deber&aacute;n reservarse los martes o jueves de la semana anterior en horario de 10.30 a 14h en el tel&eacute;fono 674 387 924. M&iacute;nimo de ni&ntilde;s por taller 5, un m&aacute;ximo de 12/15. En caso de no haber n&uacute;meros suficientes se avisar&aacute; a los padres el jueves anterior por la tarde.
        </p>
        
        </div><!--pagina_post -->
        
        <?php endwhile; ?>


	<?php endif; ?>


        </div> <!-- /contenedor_central_contenido-->
        
        
        <?php get_sidebar(); ?>

        

</div> <!--end contenedor_central -->



<?php get_footer(); ?>
